package com.hackyeah.server.data

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Notifications")
data class Event(
        @Id
        val id:String,
        val name:String
)