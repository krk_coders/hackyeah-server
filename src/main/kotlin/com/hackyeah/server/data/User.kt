package com.hackyeah.server.data

import com.hackyeah.server.enums.UserState
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "Users")
data class User(
        @Id
        val id:String,
        val firstName:String,
        val lastName: String,
        val state: UserState,
        val events: List<Event>
)