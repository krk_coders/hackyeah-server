package com.hackyeah.server.api

import com.hackyeah.server.data.Event
import com.hackyeah.server.repository.EventRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import kotlin.streams.toList

@RestController
@RequestMapping(value = "/api/event")
class EventResources @Autowired constructor(private val eventRepository: EventRepository) {

    @GetMapping(value = "/page/name")
    fun getEventsPageByName(@RequestParam("pageNumber") pageNumber: Int,
                            @RequestParam("pageSize") pageSize: Int,
                            @RequestParam("name") name: String): List<Event> {
        return this.eventRepository.findByNameLike(name, PageRequest(pageNumber, pageSize)).content
    }

    @GetMapping("/all")
    fun getAllEvents(): List<Event> {
        return this.eventRepository.findAll()
    }

    @GetMapping("/getPage")
    fun getPage(@RequestParam("pageNumber") pageNumber: Long,
                @RequestParam("eventsNumber") eventsNumber: Long): List<Event> {
        return this.eventRepository.findAll().stream()
                .skip((pageNumber - 1) * eventsNumber)
                .limit(eventsNumber).toList();
    }
}