package com.hackyeah.server.api

import com.hackyeah.server.data.User
import com.hackyeah.server.enums.UserState
import com.hackyeah.server.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.function.Predicate
import kotlin.streams.toList

@RestController
@RequestMapping(value = "/api/user")
class UserResources @Autowired constructor(private val userRepository: UserRepository) {

    @GetMapping(value = "/all", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getAllUsers(): List<User> {
        return userRepository.findAll()
    }

    @GetMapping(value = "/activeUsers", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getActiveUsers(): List<User> {
        return userRepository.findAll().stream().filter(Predicate { user -> UserState.ACTIVE.equals(user.state) }).toList();
    }
}