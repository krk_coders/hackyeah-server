package com.hackyeah.server.enums

enum class UserState {
    ACTIVE, NOTACTIVE, ONHOLD
}