package com.hackyeah.server.loaders

import com.hackyeah.server.data.Event
import com.hackyeah.server.data.User
import com.hackyeah.server.enums.UserState
import com.hackyeah.server.repository.EventRepository
import com.hackyeah.server.repository.UserRepository
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component

@Component
class DataLoader @Autowired constructor(private val eventRepository: EventRepository,
                                        private val userRepository: UserRepository) : CommandLineRunner {


    override fun run(vararg args: String?) {
        saveSampleNotifications()
        saveSampleUsers()
    }

    private fun saveSampleUsers() {
        userRepository.deleteAll()
        userRepository.save(
                listOf(
                        User(ObjectId.get().toString(), "Rafal", "Jankowski",UserState.ACTIVE, eventRepository.findAll()),
                        User(ObjectId.get().toString(), "Karol", "Klimonczyk",UserState.NOTACTIVE, listOf(eventRepository.findByName("abc"))),
                        User(ObjectId.get().toString(), "Bartek", "Golabek",UserState.ACTIVE, listOf(eventRepository.findByName("abc"))),
                        User(ObjectId.get().toString(), "Michal", "Drak",UserState.ACTIVE, listOf(eventRepository.findByName("abc"))),
                        User(ObjectId.get().toString(), "Dominik", "Kownacki",UserState.ACTIVE, listOf(eventRepository.findByName("abc"))),
                        User(ObjectId.get().toString(), "Jan", "Kowalski",UserState.NOTACTIVE, listOf(eventRepository.findByName("abc"))),
                        User(ObjectId.get().toString(), "Agata", "UberKoder",UserState.ACTIVE, listOf(eventRepository.findByName("abc"))),
                        User(ObjectId.get().toString(), "Rafal", "UberUber",UserState.ACTIVE, listOf(eventRepository.findByName("fasdfs"))
                        )
                ))
    }

    private fun saveSampleNotifications() {
        eventRepository.deleteAll()
        eventRepository.save(
                listOf(
                        Event(ObjectId.get().toString(), "abc"),
                        Event(ObjectId.get().toString(), "fasdfs"),
                        Event(ObjectId.get().toString(), "dsffsd"),
                        Event(ObjectId.get().toString(), "fsdfsd"),
                        Event(ObjectId.get().toString(), "dfsgdfgf"),
                        Event(ObjectId.get().toString(), "gdfgfd"),
                        Event(ObjectId.get().toString(), "gfd"),
                        Event(ObjectId.get().toString(), "asbvcbvcdf"),
                        Event(ObjectId.get().toString(), "bcvbcv"),
                        Event(ObjectId.get().toString(), "asvcbdf"),
                        Event(ObjectId.get().toString(), "asbcvbdf"),
                        Event(ObjectId.get().toString(), "assaddf"),
                        Event(ObjectId.get().toString(), "asdf"),
                        Event(ObjectId.get().toString(), "asfdsvcxdf"),
                        Event(ObjectId.get().toString(), "asasdasdf"),
                        Event(ObjectId.get().toString(), "asdf"),
                        Event(ObjectId.get().toString(), "asvcxvxcvdf"),
                        Event(ObjectId.get().toString(), "asdf"),
                        Event(ObjectId.get().toString(), "ascxvdf"),
                        Event(ObjectId.get().toString(), "ascvxdf"),
                        Event(ObjectId.get().toString(), "asvgfdf"),
                        Event(ObjectId.get().toString(), "asbdf"),
                        Event(ObjectId.get().toString(), "asdhfgf"),
                        Event(ObjectId.get().toString(), "asvcxdf"),
                        Event(ObjectId.get().toString(), "asdf"),
                        Event(ObjectId.get().toString(), "asvxcdf"),
                        Event(ObjectId.get().toString(), "ashgfdf"),
                        Event(ObjectId.get().toString(), "asdhfgf"),
                        Event(ObjectId.get().toString(), "asgfdf"),
                        Event(ObjectId.get().toString(), "ashgfdf"),
                        Event(ObjectId.get().toString(), "ashgfdf"),
                        Event(ObjectId.get().toString(), "asdhfgf"),
                        Event(ObjectId.get().toString(), "asvcxdf"),
                        Event(ObjectId.get().toString(), "asdf"),
                        Event(ObjectId.get().toString(), "asvxcdf"),
                        Event(ObjectId.get().toString(), "ashgfdf"),
                        Event(ObjectId.get().toString(), "asdhfgf"),
                        Event(ObjectId.get().toString(), "asgfdf"),
                        Event(ObjectId.get().toString(), "ashgfdf"),
                        Event(ObjectId.get().toString(), "ashgfdf"),
                        Event(ObjectId.get().toString(), "asdhfgf")
                ))
    }
}