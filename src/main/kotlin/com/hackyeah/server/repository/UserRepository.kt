package com.hackyeah.server.repository

import com.hackyeah.server.data.User
import org.springframework.data.mongodb.repository.MongoRepository

interface UserRepository : MongoRepository<User, String> {

}