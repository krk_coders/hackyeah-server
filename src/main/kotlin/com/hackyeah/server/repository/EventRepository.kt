package com.hackyeah.server.repository

import com.hackyeah.server.data.Event
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository

interface EventRepository : MongoRepository<Event, String> {

    fun findById(id: String): Event

    fun findByName(name: String): Event

    fun findByNameLike(name: String, pageable: Pageable): Page<Event>
}